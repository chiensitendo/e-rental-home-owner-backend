package com.e_rental.owner.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserListResponse {

    @JsonProperty
    public String name;
}
